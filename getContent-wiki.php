<?php

$pageId = $_POST['pageId'];
$string = $_POST['pageTitle'];
$string = str_replace(" ", "%20", $string);
$string = str_replace("%11", "'", $string);

$fileName1 = "https://en.wikipedia.org/w/api.php?action=query&format=json&prop=cirrusdoc&list=&continue=%7C%7Ccategoryinfo%7Cmapdata%7Cwbentityusage%7Cflagged%7Cfileusage%7Crevisions%7Ctranscludedin&titles=$string";
$fileName2 = "https://en.wikipedia.org/w/api.php?action=query&format=json&prop=pageimages%7Ccategories&list=&titles=$string";

$fileContent1 = file_get_contents($fileName1);
$dataContent1 = json_decode($fileContent1);
$fileContent2 = file_get_contents($fileName2);
$dataContent2 = json_decode($fileContent2);
  
$imgPath = '';
if (isset($dataContent2->query->pages->$pageId->thumbnail->source)) { 
    $imgPath = $dataContent2->query->pages->$pageId->thumbnail->source;
}else{
    $imgPath = 'no_image.png';
}

//echo "$pageId <br/>$pageTitle<br/>";
//echo "<pre>";
//print_r($dataContent1->query->pages);
//print_r($dataContent2->query->pages);
//exit;

?>
<table class="table">
    <thead>
        <tr>
            <th>Page ID</th>
            <th>Title</th>
            <!--<th>Template</th>-->
            <!--<th>Auxiliary Text</th>-->
            <!--<th>Redirect</th>-->
            <!--<th>Heading</th>-->
            <!--<th>External Link</th>-->
            <!--<th>Category</th>-->
            <!--<th>Outgoing Link</th>-->
            <th>Description</th>
            <th>Image</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($dataContent1->query->pages as $row) {
            $text = (!empty($row->cirrusdoc[0]->source->text)) ? $row->cirrusdoc[0]->source->text : '';
//            $arrTemplate = "";
//            $auxiliary_text = "";
//            $redirect = "";
//            $heading = "";
//            $external_link = "";
//            $category = "";
//            $outgoing_link = "";
//
//            for ($d = 0; $d < count($row->cirrusdoc[0]->source->template); $d++) {
//                $arrTemplate .= $row->cirrusdoc[0]->source->template[$d] . ', <br>';
//            }
//            for ($d = 0; $d < count($row->cirrusdoc[0]->source->auxiliary_text); $d++) {
//
//                $auxiliary_text .= $row->cirrusdoc[0]->source->auxiliary_text[$d] . ', <br><br><br>';
//            }
//            for ($d = 0; $d < count($row->cirrusdoc[0]->source->redirect); $d++) {
//
//                $redirect .= $row->cirrusdoc[0]->source->redirect[$d]->title . ', <br>';
//            }
//            for ($d = 0; $d < count($row->cirrusdoc[0]->source->heading); $d++) {
//
//                $heading .= $row->cirrusdoc[0]->source->heading[$d] . ', <br>';
//            }
//            for ($d = 0; $d < count($row->cirrusdoc[0]->source->external_link); $d++) {
//
//                $external_link .= $row->cirrusdoc[0]->source->external_link[$d] . ', <br>';
//            }
//            for ($d = 0; $d < count($row->cirrusdoc[0]->source->category); $d++) {
//
//                $category .= $row->cirrusdoc[0]->source->category[$d] . ', <br>';
//            }
//            for ($d = 0; $d < count($row->cirrusdoc[0]->source->outgoing_link); $d++) {
//
//                $outgoing_link .= $row->cirrusdoc[0]->source->outgoing_link[$d] . ', <br>';
//            }
            ?>
        <tr>
            <td><?php echo $pageId; ?></td>
            <td><?php echo $row->title; ?></td>
            <!--<td><?php // echo $arrTemplate; ?></td>-->
            <!--<td><?php // echo $auxiliary_text; ?></td>-->
            <!--<td><?php // echo $redirect; ?></td>-->
            <!--<td><?php // echo $heading; ?></td>-->
            <!--<td><?php // echo $external_link; ?></td>-->
            <!--<td><?php // echo $category; ?></td>-->
            <!--<td><?php // echo $outgoing_link; ?></td>-->
            <td><?php echo $text; ?></td>
            <td><img width="100" height="100" src="<?php echo $imgPath; ?>"></td>
        </tr>        
        <?php } ?>
    </tbody>
</table>