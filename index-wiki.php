<html>
    <head>
        <link rel="stylesheet" href="bootstrap.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <style>
        .ui-autocomplete-loading {
          background: white url("images/ui-anim_basic_16x16.gif") right center no-repeat;
        }
        </style>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
        $( function() {
          function log( message ) {
            $( "<div>" ).text( message ).prependTo( "#log" );
            $( "#log" ).scrollTop( 0 );
          }
 
          $( "#searchData" ).autocomplete({
                    source: "search-wiki.php",
                    minLength: 2,
                    select: function(event, ui) {
                        log("Selected: " + ui.item.value + " aka " + ui.item.id);
                    }
                }).autocomplete("instance")._renderItem = function(ul, item) {
                    return $("<li class='pageTitle' data-page-id='"+ item.pageId +"' data-page-title='"+ item.title+"'>")
                            .append("<div class='sss' style='width: 420px;'><img src="+ item.imgPath +" alt='No Image'> &nbsp; <b>" + item.label + "</b><br>" + item.details + "</div><hr/>")
                            .appendTo(ul);
                };
            });
        </script>
    </head>
    <body>
        <form method="post" class="form-horizontal">
            <br/>
            <div class="row">
                <div class="col-md-12">
                    <label class="col-sm-3 control-label"> Search </label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="searchData" placeholder="Enter key word...">
                    </div>
                    <div class="col-sm-2">
                        <input type="button" class="btn btn-primary" value="Search" id="searchButton"> 
                    </div>
                    <div class="col-sm-1">
                        <img src="giphy.gif" id="loadinimage" width="50" class="hide"> 
                    </div>
                </div>
            </div>
        </form>

        <div id="dataAppend"></div>

        <script src="jquery-2.2.3.min.js"></script>
        <script src="bootstrap.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $('body').on('click', '.pageTitle', function() {
                //alert('hi')
                var pageId = $(this).attr('data-page-id');
                var pageTitle = $(this).attr('data-page-title');
                var dataArr = {'pageId' : pageId, 'pageTitle' : pageTitle };
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').val(),
                    },
                    url: "getContent-wiki.php",
                    //data: {'data': dataArr },
                    data: dataArr,
                    success: function(data) {
                        $('#loadinimage').addClass('hide');
                        $("#dataAppend").html(data);
                    }
                });
            });
            
            /* Below function are not required */
//            $('#searchButton').click(function() {
//                $('#loadinimage').removeClass('hide');
//                ($('#searchButton').val())
//                var textValue = $('#searchData').val();
//                $.ajax({
//                    type: "POST",
//                    headers: {
//                        'X-CSRF-TOKEN': $('input[name="_token"]').val(),
//                    },
//                    url: "getContent.php",
//                    data: {'data': textValue},
//                    success: function(data) {
//                        $('#loadinimage').addClass('hide');
//                        $("#dataAppend").html(data);
//                    }
//                });
//            });

        </script>
    </body>
</html>